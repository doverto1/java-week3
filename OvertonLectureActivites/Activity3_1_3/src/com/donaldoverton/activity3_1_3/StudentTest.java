package com.donaldoverton.activity3_1_3;


public class StudentTest {
	public static void main(String[] args)
	{
		Student Student1 = new Student();
		Student Student2 = new Student("John Doe", 20, 'F', "IT");


		//display the information for the first student
		Student1.displayInfo();

		//display the information for the second student
		Student2.displayInfo();

	}
}
