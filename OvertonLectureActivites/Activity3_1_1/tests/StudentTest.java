import org.junit.Test;

import static org.junit.Assert.*;


public class StudentTest {
	@Test
	public void constructorTest() throws Exception {
		Student student = new Student();
		assertEquals( null, student.getName());
		assertEquals(0, student.getAge());
		assertEquals(0, student.getGender());
		assertEquals(false, student.isAnITMajor());
		assertEquals(null, student.getMajor());

		Student student1 = new Student("Joe",12, true,'M');
		assertEquals("Joe", student1.getName());
		assertEquals(12,student1.getAge());
		assertEquals(true, student1.isAnITMajor());
		assertEquals('M', student1.getGender());
	}

	@Test
	public void getNameTest() throws Exception {
		Student student = new Student();
		student.setName("Don");
		assertEquals("Don",student.getName());
	}


	@Test
	public void getAgeTest() throws Exception {
		Student student = new Student();
		student.setAge(1975,2017);
		assertEquals(42, student.getAge());
	}


	@Test
	public void iTMajorTest() throws Exception {
		Student student = new Student();
		student.setMajor("IT");
		assertEquals("IT",student.getMajor());
		assertEquals(true, student.isAnITMajor());

	}

	@Test
	public void businessMajorTest() throws Exception {
		Student student = new Student();
		student.setMajor("Business");
		assertEquals("Business",student.getMajor());
		assertEquals(false, student.isAnITMajor());
	}

	@Test
	public void getGenderTest() throws Exception {
		Student student = new Student();
		student.setGender('M');
		assertEquals('M', student.getGender());

	}

}