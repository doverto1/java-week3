
public class Student {
	String name;
	int age;
	boolean isAnITMajor;
	char gender;
	String major;

	public Student(){
		String name = " ";
		int age = 0;
		boolean isAnITMajor = false;
		char gender = 'F';
		String major = " ";

	}

	public Student(String name, int age, boolean isAnITMajor, char gender){
		this.name = name;
		this.age = age;
		this.isAnITMajor = isAnITMajor;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int yearOfBirth, int currentYear) {
		if(yearOfBirth != 0 && currentYear != 0 ){
			this.age =  currentYear - yearOfBirth;
		}else{
			this.age = 0;
		}
	}

	public boolean isAnITMajor() {
		return isAnITMajor;
	}

	public void setAnITMajor(boolean anITMajor) {
		isAnITMajor = anITMajor;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		if(major == "IT"){
			this.isAnITMajor = true;
		}
		this.major = major;
	}
}

