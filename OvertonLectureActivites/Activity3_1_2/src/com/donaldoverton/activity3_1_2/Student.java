package com.donaldoverton.activity3_1_2;

public class Student {
	String name;
	int age;
	boolean isAnITMajor;
	char gender;
	String major;

	public Student(){
		String name = " ";
		int age = 0;
		boolean isAnITMajor = false;
		char gender = 'F';
		String major = " ";

	}

	/**
	 *
	 * Student Student1 = new Student();
	 * Student Student2 = new Student("John Doe", 20, 'F', "IT");
	 */


	public Student(String name, int age, char gender, String major){
		this.name = name;
		setAge(age);
		this.age = age;
		this.major = major;
		setMajor(major);
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int setAgeYears(int yearOfBirth, int currentYear){
		if(yearOfBirth != 0 && currentYear != 0 ){
			return currentYear - yearOfBirth;
		}else{
			return 0;
		}
	}

	public boolean isAnITMajor() {
		return isAnITMajor;
	}

	public void setAnITMajor(boolean anITMajor) {
		isAnITMajor = anITMajor;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		if(major == "IT"){
			this.isAnITMajor = true;
		}
		this.major = major;
	}
}
